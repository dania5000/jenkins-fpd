для сохранения докер образа нужно выполнить три команды:

docker login registry.gitlab.com
docker tag {IMAGE_ID} registry.gitlab.com/dania5000/jenkins-fpd:{TAG}
docker push registry.gitlab.com/dania5000/jenkins-fpd:{TAG}   